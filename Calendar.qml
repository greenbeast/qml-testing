GridLayout {
	columns: 7
	
	DayOfWeekRow {
		locale: grod.local
		Layout.column: 1
		Layout.fillWidth: true
	}
	
	WeekNumberColumn {
		month: grid.month
		year: grid.year
		locale: grid.locale
		Layout.fillHeight: true
	}
	
	MonthGrid{
		id: grid
		month: Calendar.July
		year: 2020
		locale: Qt.locale("en_US")

		Layout.fillWidth: true
		Layout.fillHeight: true
	}
}
